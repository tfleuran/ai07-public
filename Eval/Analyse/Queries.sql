SELECT
  P.Age as AGE,
  Count(*)::decimal/ (select count(*) from fbdl.dim_people where age = P.age group by age) as NB_CALLS_AGAINT_NB_CUSTOMERS,
  Count(*) as NB_CALLS,
  (select count(*) from fbdl.dim_people where age = P.age group by age) as NB_CUSTOMERS
FROM fbdl.Calls C
LEFT JOIN fbdl.dim_people P ON P.row_num = C.FK_People
GROUP BY P.Age ORDER BY P.AGE;

select
	Age,
	Count(*)
from fbdl.dim_people
where Age notnull
group by age
order by age;

select
	L.departementcode as Departement_Code,
	count(*)::decimal/(select population from fbdl.dim_location where departementcode = L.departementcode) * 10000 as NB_CALLS_AGAINST_POPULATION,
	L.departementlabel as Departement_Name
from fbdl.calls C
left join fbdl.dim_location L on L.row_num = C.fk_location
group by L.departementcode, L.departementlabel
order by L.departementcode;

select
	P.gender as GENDER,
	Count(*)::decimal/(select count(*) from fbdl.dim_people where gender = P.gender) as CALLS_AGAINST_GENDER_BY_NB_CUSTOMERS,
	Count(*) as CALLS_AGAINST_GENDER,
	(select count(*) from fbdl.dim_people where gender = P.gender) as NB_CUSTOMER_BY_GENDER
from fbdl.calls C
left join fbdl.dim_people P on P.row_num = C.fk_people
group by P.gender;

select
	D.joursemaine AS Day_OF_WEEK,
	count(*)::decimal/(select count(*) from fbdl.dim_date where joursemaine=D.joursemaine group by joursemaine) NB_CALLS_DOW_BY_NB_OF_DATES,
	count(*) as NB_CALLS_AGAINST_DOW,
	(select count(*) from fbdl.dim_date where joursemaine=D.joursemaine group by joursemaine) NB_Dates_DOW
from fbdl.calls C
left join fbdl.dim_date D on D.row_num = C.fk_date
group by D.joursemaine
order by D.joursemaine;

select
	P.age as AGE,
	(sum(C.disconnection)::decimal/count(*)) as NB_Disconnections_Against_NB_CALLS,
	count(*) NB_CALLS,
	sum(C.disconnection) as NB_Disconnections,
	(select count(*) from fbdl.dim_people where age = P.age group by age) NB_Customers_BY_age
from fbdl.calls C
left join fbdl.dim_people P on P.row_num = C.fk_people
group by P.age
order by P.age
;

select * from fbdl.calls;


