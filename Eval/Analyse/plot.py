import matplotlib.pyplot as plt
import psycopg2
import sys


def bdd():
    try:
        conn = psycopg2.connect(
            host="tuxa.sme.utc",
            dbname="dbbdd1p006",
            user="bdd1p006",
            password="NOP",
        )
        cur = conn.cursor()
        return conn, cur
    except:
        print("Connexion impossible")
        sys.exit()


if __name__ == "__main__":
    conn, cur = bdd()
    sql = "\
            select\
                P.age as AGE,\
                (sum(C.disconnection)::decimal/count(*)) as NB_Disconnections_Against_NB_CALLS,\
                count(*) NB_CALLS,\
                sum(C.disconnection) as NB_Disconnections,\
                (select count(*) from fbdl.dim_people where age = P.age group by age) NB_Customers_BY_age\
            from fbdl.calls C\
            left join fbdl.dim_people P on P.row_num = C.fk_people\
            group by P.age\
            order by P.age\
          ;"
    cur.execute(sql)
    x = []
    y = []
    for row in cur:
        x.append(row[0])
        y.append(row[1])
    print(x)
    print(y)
    plt.bar(x, y)
    plt.ylabel("Nombre de déconnexions")
    plt.xlabel("Age")
    plt.title("Nombre moyen de déconnexions en fonction de l'âge du client")
    plt.show()
    cur.close()
    conn.close()
