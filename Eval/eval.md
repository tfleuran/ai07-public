# Evaluation du 26 mars

## ETL

La table de fait et la dimension personnes (dim_people) ont été msies dans le DATAWAREHOUSE. Toutes les contraintes n'ont pas été mises. La phase de traitement des données n'a pu être réalisée dans les temps

Concernant le reporting:
  * on trouvera dans etl/reporting/lines.csv un compte des lignes passées d'un table à une autre (interzone) permettant une vérification minimale de l'exécution
  * Je n'ai aps eu le temps de réaliser un reporting supplémentaire

## Requêtage & analyse

Le fichier ./etl/Requêtage/rapport.md contient un example de question avec un début de réponse. Comme marqué dans ce fichier, une analyse plus prodfonde doit être réalisée pour répondre correctement à la question (réaligner par rapport au nombre d'utilisateurs).
