# Analyse préliminaire des données
## FobsCalls

Fichier sans Header, ";" comme sépérateur, "" pour représenter les chaînes de caractères.

| Donnée | Qualité Perçue |
| ------ | -------------- |
| Date | Aucun Null, semble correct |
| Disconnection | Aucun Null, semble correct |
| Product | Aucun Null, semble correct |
| CustomerNumber | Aucun Null, semble correct |
| Departement | Aucun Null, semble correct |
| ReceiverNumber | Aucun Null, semble correct |
| Duration | Aucun Null, semble correct |

A priori les données sont propres.

Nombre de lignes: 4837

## FobsPhones

Fichier sans Header, ";" comme sépérateur, "" pour représenter les chaînes de caractères.

| Donnée | Qualité Perçue |
| ------ | -------------- |
| CustomerNumber | Aucun Null, semble correct |
| DateOfBirth | Aucun Null, semble correct |
| Gender | Aucun Null, semble correct |

A priori les données sont propres.

Nombre de lignes: 474

## Product

Fichier sans Header, ";" comme sépérateur, "" pour représenter les chaînes de caractères.

| Donnée | Qualité Perçue | Commentaire |
| ------ | -------------- | ----------- |
| Product | Aucun Null, semble correct | |
| Color | Aucun Null, semble correct | Chaque couleur semble être unique |
| Price | Aucun Null, semble correct | Trop détaillé pour analyse, à segmenter |
| Size | Aucun Null, semble correct | Trop détaillé pour analyse, à segmenter (ex: La taille a-t-elle une influence sur la popularité d'un produit?) |

A priori les données sont propres.

Nombre de lignes: 171

## dpt_fr

Fichier sans Header, ";" comme sépérateur, "" pour représenter les chaînes de caractères.

| Donnée | Qualité Perçue |
| ------ | -------------- |
| Departement | Aucun Null, semble correct |
| DptName | Aucun Null, semble correct |
| Population | Aucun Null, semble correct |

A priori les données sont propres.

Nombre de lignes: 95
