# Exemple de requête décisionnelle

En se basant sur l'étude des besoins, on peut arriver à la requête suivante:
*L'âge des clients a-t-il un impact sur leur nombre d'appels par notre service?*

On peut traduire cette requête sous la forme dimensionnelle suivante:
```
Nombre d'appels
/Customer(Age)
```

En se basant sur le modèle dimensionnel conçu, cette requête se traduirait en SQL de la façon suivante:
```
SELECT
  P.Age,
  Count(*)
FROM Calls C
LEFT JOIN dim_people P ON P.row_num = C.FK_Customer
GROUP BY P.Age
```
Une analyse pourrait ensuite être réalisée sous forme graphique à partir de ces résultats.
