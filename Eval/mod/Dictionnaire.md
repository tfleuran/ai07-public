# Dictionnaire de données Datawarehouse

## Liste des tables

| Nom Table | Role |
| --------- | ---- |
| Calls | Faits |
| Dim_Date | Dimension|
| Dim_People | Dimension |
| Dim_Product | Dimension |
| Dim_Location | Dimension |
## Description détaillée

### Calls
Table des faits
| Champ | Type | Commentaire |
| ----- | ---- | ----------- |
| Disconnection | Entier | |
| Duration | Entier | |
| ReceiverNumber | Texte | Stockage d'un numéro de téléphone -> Text |
| FK_Date | Entier | Ref. dim_date, 0 si Null|
| FK_Product | Entier | Ref. dim_product, 0 si Null |
| FK_People | Entier | Ref. dim_people, 0 si Null |
| FK_Location | Entier | Ref. dim_Location, 0 si Null  |

### Dim_Date
Table des dates.
| Champ | Type | Commentaire |
| ----- | ---- | ----------- |
| row_num | Entier | Clé |
| Date | Date | |
| JourSemaine | Entier | Calculé depuis Date |
| Jour | Entier | Calculé depuis Date |
| Semaine | Entier | Calculé depuis Date |
| Mois | Entier | Calculé depuis Date |
| Trimestre | Entier | Calculé depuis Date |
| Semestre | Entier | Calculé depuis Trimestre |
| Annee | Entier | Calculé depuis Date |

### Dim_People
Table des utilisateurs du service (Cutomer)
| Champ | Type | Commentaire |
| ----- | ---- | ----------- |
| row_num | Entier | Clé |
| CustomerNumber | Texte | Stockage d'un numéro de téléphone -> Text |
| DateOfBirth | Date | |
| Age | Entier | Calculé à partir de DateOfBirth |
| Gender | {'F', 'M', 'U'} | |

### Dim_Product
Table des Produits de l'entreprise
| Champ | Type | Commentaire |
| ----- | ---- | ----------- |
| row_num | Entier | Clé |
| Product | Texte | |
| Color | Texte | Pour l'instant, les couleurs sont uniques, donc attribut descriptif|
| Price | Entier | Prix exprimé en entier dans les données, donc considéré comme tel |
| PriceSegment | {'A', 'B', 'C'} | Segmentation de la gamme de prix des services proposés |
| Size | Entier | Taille exprimée en entier, donc gardé en Entier |
| SizeSegment | {'S', 'M', 'L'} | Segmentation des tailles d'appareil pour analyse en tant que facteur d'influence potentiel ? |
| Best Product | Booleen | Attribut d'agrégation: Vrai si Produit fait parti des 10% de produit ayant été le plus utilisé (indicateur=temps d'utilisation)|

### Dim_Location
Table des Lieux
| Champ | Type | Commentaire |
| ----- | ---- | ----------- |
| row_num | Entier | Clé |
| departementCode | Texte | Les données pourraient être stockée en tant qu'entier, mais des découpages avec lettre existe (source: Insee) |
| departementLabel | Texte | |
| Population | Entier | Peu exploitable pour analyse, granularité trop fine |
| PopulationSegment | {'A', 'B', 'C'} | Segmentation des départements en fonction de leur population. A>B>C |