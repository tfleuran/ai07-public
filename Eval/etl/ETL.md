# ETL

## Exécution

Sur un ordinateur de la salle, ou avec Python3 et les libs nécessaires installées, dans ce dossier:
```
python etl.py
```

## Reporting du déroulement

Voir [Rapport](./reporting/Reporting.md)
