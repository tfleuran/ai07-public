import psycopg2
import csv
import re
from datetime import datetime

def log_mod(dim, line, mod):
    with open("./reporting/mod.csv", "a") as csvfile:
        report_number = csv.writer(csvfile)
        report_number.writerow([datetime.now(), dim, line, mod])
    csvfile.close()

def tuple_to_list(tuple_init, update_list):
    for x in tuple_init:
        update_list.append(x)
    return update_list

def fbdt_transform_Product(conn, cur):

    cur_update = conn.cursor()
    cur.execute("Select * from fbdt.Product")
    for row in cur:
        up_row = []
        up_row = tuple_to_list(row, up_row)

        if re.fullmatch('REF\d*', row[1]) is None:
            up_row[1] = None
            log_mod('Product', row[0], 'product')
        else:
            up_row[1] = row[1]

        if re.fullmatch('#[\da-fA-F]{6}', row[2]) is None:
            up_row[2] = None
            log_mod('Product', row[0], 'color')
        else:
            up_row[2] = row[2]

        cur_update.execute(
            "UPDATE fbdt.Product\
                SET Product='{}',\
                Color='{}'\
                WHERE row_num = {}".format(up_row[1],up_row[2],row[0])
        )

    cur_update.close()
    conn.commit()

def fbdt_transform_FobsPhones(conn, cur):

    cur_update = conn.cursor()
    cur.execute("Select * from fbdt.FobsPhones")
    for row in cur:
        up_row = []
        up_row = tuple_to_list(row, up_row)

        if re.fullmatch('[FMU]', row[3]) is None:
            up_row[3] = None
            log_mod('People', row[0], 'product')
        else:
            up_row[3] = row[3]

        cur_update.execute(
            "UPDATE fbdt.FobsPhones\
                SET Gender='{}'\
                WHERE row_num = {}".format(up_row[3],row[0])
        )

    cur_update.close()
    conn.commit()

def fbdt_transform_FobsCalls(conn, cur):
    cur_update = conn.cursor()
    cur.execute("Select * from fbdt.FobsCalls")

    cur_check = conn.cursor()
    for row in cur:
        up_row = []
        up_row = tuple_to_list(row, up_row)

        cur_check.execute(
            "Select * from fbdt.Product where Product='{}'".format(row[3])
        )
        if cur_check.rowcount !=1:
            up_row[3] = None
            log_mod('Calls', row[0], 'product')
        else:
            up_row[3] = row[3]

        cur_check.execute(
            "Select * from fbdt.FobsPhones where CustomerNumber='{}'".format(row[4])
        )
        if cur_check.rowcount !=1:
            up_row[4] = None
            log_mod('Calls', row[0], 'people')
        else:
            up_row[4] = row[4]

        cur_check.execute(
            "Select * from fbdt.dpt_fr where Departement='{}'".format(row[5])
        )
        if cur_check.rowcount !=1:
            up_row[5] = None
            log_mod('Calls', row[0], 'location')
        else:
            up_row[5] = row[5]

        cur_update.execute(
            "UPDATE fbdt.FobsCalls\
                SET product='{}',\
                CustomerNumber='{}',\
                departement='{}'\
                WHERE row_num = {}".format(up_row[3],up_row[4],up_row[5],row[0])
        )

    cur_check.close()
    cur_update.close()
    conn.commit()


def fbdt_transform_data(conn, cur):
    fbdt_transform_Product(conn, cur)
    fbdt_transform_FobsPhones(conn, cur)
    fbdt_transform_FobsCalls(conn, cur)
