BEGIN;

CREATE TABLE fbdl.dim_people AS
SELECT * from fbdt.dim_people;

CREATE TABLE fbdl.dim_date AS
SELECT * from fbdt.dim_date;

CREATE TABLE fbdl.dim_product AS
SELECT * from fbdt.dim_product;

CREATE TABLE fbdl.dim_location AS
SELECT * from fbdt.dim_location;

CREATE TABLE fbdl.Calls AS
SELECT * from fbdt.fac_Calls;

------ CONTRAINTES
ALTER TABLE fbdl.dim_people
  ALTER COLUMN CustomerNumber TYPE TEXT,
  ALTER COLUMN DateOfBirth TYPE Date,
  ALTER COLUMN Age TYPE INTEGER,
  ALTER COLUMN Gender TYPE Varchar(1),
  ADD CONSTRAINT dim_people_primary_key PRIMARY KEY (row_num),
  ADD CONSTRAINT dim_people_gender CHECK (GENDER SIMILAR TO '[FMU]'),
  ADD CONSTRAINT dim_people_age CHECK (Age > 0)
  ;

ALTER TABLE fbdl.dim_date
  ALTER COLUMN date TYPE date,
  ALTER COLUMN JourSemaine TYPE INTEGER,
  ALTER COLUMN JOUR TYPE INTEGER,
  ALTER COLUMN Semaine TYPE INTEGER,
  ALTER COLUMN Mois TYPE INTEGER,
  ALTER COLUMN TRIMESTRE TYPE INTEGER,
  ALTER COLUMN Semestre TYPE INTEGER,
  ALTER COLUMN Annee TYPE INTEGER,
  ADD CONSTRAINT dim_date_primary_key PRIMARY KEY (row_num),
  ADD CONSTRAINT dim_date_JourSemaine CHECK (JourSemaine >= 1 and JourSemaine <= 7),
  ADD CONSTRAINT dim_date_Semaine CHECK (Semaine >= 1 and Semaine <= 53),
  ADD CONSTRAINT dim_date_Mois CHECK (Mois >= 1 and Mois <= 12),
  ADD CONSTRAINT dim_date_Trimestre CHECK (Trimestre >= 1 and Trimestre <= 4),
  ADD CONSTRAINT dim_date_Semestre CHECK (Semestre >= 1 and Semestre <= 2),
  ADD CONSTRAINT dim_date_Annee CHECK (Annee >= 1)
  ;

ALTER TABLE fbdl.dim_product
  ALTER COLUMN Product TYPE TEXT,
  ALTER COLUMN Color TYPE TEXT,
  ALTER COLUMN Price TYPE INTEGER,
  ALTER COLUMN PriceSegment TYPE Varchar(1),
  ALTER COLUMN Size TYPE INTEGER,
  ALTER COLUMN SizeSegment TYPE Varchar(1),
  ALTER COLUMN BestProduct TYPE BOOLEAN,
  ADD CONSTRAINT dim_product_primary_key PRIMARY KEY (row_num),
  ADD CONSTRAINT dim_product_product CHECK (product similar to 'REF\d*'),
  ADD CONSTRAINT dim_product_color CHECK (color similar to '#[\da-fA-F]{6}'),
  ADD CONSTRAINT dim_product_priceSegment CHECK (PriceSegment SIMILAR TO '[ABC]'),
  ADD CONSTRAINT dim_product_SizeSegment CHECK (SizeSegment similar to '[SML]')
  ;

ALTER TABLE fbdl.dim_location
  ALTER COLUMN DepartementCode TYPE TEXT,
  ALTER COLUMN DepartementLabel TYPE TEXT,
  ALTER COLUMN Population TYPE INTEGER,
  ALTER COLUMN PopulationSegment TYPE Varchar(1),
  ADD CONSTRAINT dim_location_primary_key PRIMARY KEY (row_num),
  ADD CONSTRAINT dim_location_population CHECK (Population > 0),
  ADD CONSTRAINT dim_location_populationSegment CHECK (PopulationSegment similar to '[ABC]')
  ;

ALTER TABLE fbdl.Calls
  ALTER COLUMN Disconnection TYPE INTEGER,
  ALTER COLUMN Duration TYPE INTEGER,
  ALTER COLUMN ReceiverNumber TYPE TEXT,
  ADD CONSTRAINT calls_fk_Date FOREIGN KEY (FK_Date) REFERENCES fbdl.dim_Date(row_num),
  ADD CONSTRAINT calls_fk_Product FOREIGN KEY (FK_Product) REFERENCES fbdl.dim_Product(row_num),
  ADD CONSTRAINT calls_fk_People FOREIGN KEY (FK_People) REFERENCES fbdl.dim_People(row_num),
  ADD CONSTRAINT calls_fk_Location FOREIGN KEY (FK_Location) REFERENCES fbdl.dim_Location(row_num)
  ;


COMMIT;
