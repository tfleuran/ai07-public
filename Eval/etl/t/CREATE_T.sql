BEGIN;
SET search_path TO fbdt;
CREATE TABLE IF NOT EXISTS FobsCalls(
	row_num INTEGER,
	Date date,
	Disconnection SmallInt,
	Product TEXT,
	CustomerNumber TEXT,
  Departement TEXT,
  ReceiverNumber TEXT,
  Duration INTEGER
);

CREATE TABLE IF NOT EXISTS FobsPhones(
  row_num INTEGER,
	CustomerNumber TEXT,
	DateOfBirth Date,
	Gender VARCHAR(1)
);

CREATE TABLE IF NOT EXISTS Product(
	row_num INTEGER,
	Product TEXT,
	COlor TEXT,
	Price INTEGER,
	Size INTEGER
);

CREATE TABLE IF NOT EXISTS dpt_fr(
	row_num INTEGER,
	Departement TEXT,
	dptname TEXT,
	population INTEGER
);

COMMIT;
