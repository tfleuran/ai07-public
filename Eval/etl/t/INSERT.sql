BEGIN;
INSERT INTO fbdt.FobsCalls
  Select
  row_number() over(),
  CASE WHEN date SIMILAR TO '\d{4}-\d{2}-\d{2}'
      THEN TO_DATE(date, 'YYYY-MM-DD')
      ELSE NULL
  END,
  CASE WHEN Disconnection SIMILAR TO '\d*'
      THEN CAST(Disconnection AS SmallInt)
      ELSE NULL
  END,
  Product,
  CustomerNumber,
  Departement,
  ReceiverNumber,
  CASE WHEN Duration SIMILAR TO '\d*'
      THEN CAST(Duration AS INTEGER)
      ELSE NULL
  END
  from fbde.FobsCalls;

INSERT INTO fbdt.FobsPhones
  Select
    row_number() over(),
    CustomerNumber,
    CASE WHEN DateOfBirth SIMILAR TO '\d{4}-\d{2}-\d{2}'
        THEN TO_DATE(DateOfBirth, 'YYYY-MM-DD')
        ELSE NULL
    END,
    Gender
    from fbde.FobsPhones;

INSERT INTO fbdt.Product
    Select
        row_number() over(),
        Product,
        Color,
        CASE WHEN Price similar to '\d*'
            THEN CAST(Price AS INTEGER)
            ELSE NULL
        END,
        CASE WHEN Size similar to '\d*'
            THEN CAST(Size AS INTEGER)
            ELSE NULL
        END
    FROM fbde.Product;


INSERT INTO fbdt.dpt_fr
    Select
        row_number() over(),
        Departement,
        dptName,
        CASE WHEN Population similar to '\d*'
            THEN CAST(Population AS INTEGER)
            ELSE NULL
        END
    FROM fbde.dpt_fr;

COMMIT;
