BEGIN;

SET search_path TO fbdt;

CREATE View dim_people AS
Select 
  A.*
FROM (
  SELECT
    0 as row_num
    ,null as CustomerNumber
    ,null as DateOfBirth
    ,null as Age
    ,null as Gender
  UNION
  SELECT
    P.row_num as row_num
    ,P.CustomerNumber AS CustomerNumber
    ,P.DateOfBirth AS DateOfBirth
    ,EXTRACT(YEAR FROM age(P.DateOfBirth)) AS Age
    ,P.Gender
  FROM FobsPhones P
) A
ORDER BY A.row_num;


CREATE VIEW dim_date AS
Select A.*
from(
  SELECT
    0 AS row_num
    ,null AS DATE
    ,null AS JourSemaine
    ,null AS Jour
    ,null AS Semaine
    ,null AS Mois
    ,null AS Trimestre
    ,null AS Semestre
    ,null AS Annee
  UNION
  Select
    row_number() over() AS row_num
    ,Date AS DATE
    ,EXTRACT(isodow FROM date) AS JourSemaine
    ,EXTRACT(day FROM date) AS Jour
    ,EXTRACT(week FROM date) AS Semaine
    ,EXTRACT(month FROM date) AS Mois
    ,EXTRACT(Quarter FROM date) AS Trimestre
    ,CASE
      WHEN EXTRACT(Quarter FROM date) = 1  OR EXTRACT(Quarter FROM date) = 2
        THEN 1
      WHEN EXTRACT(Quarter FROM date) = 3  OR EXTRACT(Quarter FROM date) = 4
        THEN 2
    END AS Semestre
    ,EXTRACT(Year FROM date) AS Annee
  FROM FobsCalls
  WHERE date is not NULL
  GROUP BY date
) A
ORDER BY A.row_num
;

CREATE VIEW dim_product AS
SELECT
  A.*
from(
  Select
    0 AS row_num
    ,null AS Product
    ,null AS Color
    ,null AS Price
    ,null AS PriceSegment
    ,null AS Size
    ,null AS SizeSegment
    ,null AS BestProduct
  UNION
  SELECT
    row_number() over()
    ,Product
    ,Color
    ,Price
    ,CASE
      WHEN Price <= 500 THEN 'A'
      WHEN Price > 500 AND PRICE <= 1000 THEN 'B'
      WHEN Price > 1000 THEN 'C'
    END
    ,Size
    ,CASE
      WHEN Size <= 100 THEN 'S'
      WHEN Size > 100 AND SIZE <= 200 THEN 'M'
      WHEN Size > 200 THEN 'L'
    END
    ,False
  from Product
) A
ORDER BY A.row_num;

CREATE VIEW dim_location AS
SELECT
  A.*
FROM(
  SELECT
    0 as row_num
    ,null AS departementCode
    ,null AS departementLabel
    ,null AS Population
    ,null AS PopulationSegment
  UNION
  Select
    row_number() over()
    ,Departement
    ,dptName
    ,Population
    ,CASE 
      WHEN Population > 600000 THEN 'A'
      WHEN Population <= 600000 AND Population > 300000 THEN 'B'
      WHEN Population <= 300000 THEN 'C'
    END
  FROM dpt_fr
) A
ORDER BY A.row_num;


CREATE View fac_Calls AS
Select
  C.Disconnection AS Disconnection
  ,C.Duration AS Duration
  ,C.ReceiverNumber AS ReceiverNumber
  ,L.row_num AS FK_Location
  ,D.row_num AS FK_Date
  ,Pr.row_num AS FK_Product
  ,P.row_num AS FK_People
FROM FobsCalls C
LEFT JOIN dim_people P ON C.CustomerNumber = P.CustomerNumber
LEFT JOIN dim_date D ON C.Date = D.Date
LEFT JOIN dim_product Pr ON C.Product = Pr.Product
LEFT JOIN dim_location L ON C.Departement = L.DepartementCode
;

COMMIT;
