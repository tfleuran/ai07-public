# Métriques
## Log de dernière exécution

[Log](./lastrun.log)

## Log des modifications en Zone T

Les modifications sur les données dans la zone T, exécutées en Python, sont log dans le fichier [mod.csv](./mod.csv).

Si le fichier est vide, c'est qu'il n'y a pas eu de modification.

## Nombre de lignes transmises dans l'ETL

### FobsCalls

![](./Calls.png)

### FobsPhones
Une ligne ajoutée dans la vue et ensuite pour gérer les NULL.

![](./People.png)

### Product
Une ligne ajoutée dans la vue et ensuite pour gérer les NULL.

![](./Product.png)

### Dpt_fr
Une ligne ajoutée dans la vue et ensuite pour gérer les NULL.

![](./Location.png)
