BEGIN;
SET search_path TO fbde;
CREATE TABLE IF NOT EXISTS fbde.FobsCalls(
	Date TEXT,
	Disconnection TEXT,
	Product TEXT,
	CustomerNumber TEXT,
  Departement TEXT,
  ReceiverNumber TEXT,
  Duration TEXT
);

CREATE TABLE IF NOT EXISTS fbde.FobsPhones(
	CustomerNumber TEXT,
	DateOfBirth TEXT,
	Gender TEXT
);

CREATE TABLE IF NOT EXISTS fbde.Product(
	Product TEXT,
	COLOR TEXT,
	PRICE TEXT,
	SIZE TEXT
);

CREATE TABLE IF NOT EXISTS fbde.dpt_fr(
	Departement TEXT,
	DptName TEXT,
	Population TEXT
);

COMMIT;
