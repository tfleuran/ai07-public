import psycopg2
import sys
import re
import matplotlib.pyplot as plt
from datetime import datetime
import csv
import numpy as np

from fbdt import fbdt_transform_data

# from fbdt import fbdt_create_tables, fbdt_import_data, fbdt_transform_data

# C'est très peu élégant, mais manque de temps
def compare(i, j):
    if tuple(i) == j:
        return True
    else:
        return False


def bdd():
    try:
        conn = psycopg2.connect(
            host="tuxa.sme.utc",
            dbname="dbbdd1p006",
            user="bdd1p006",
            password="Nop",
        )
        cur = conn.cursor()
        return conn, cur
    except:
        print("Connexion impossible")
        sys.exit()

def log_count(source, zone, dim, header_lines=0):
    with open("./reporting/lines.csv", "a") as csvfile:
        report_number = csv.writer(csvfile)

        if re.fullmatch("fbd.*", source) is not None:
            sql = "Select Count(*) from {};".format(source)
            cur.execute(sql)
            row = cur.fetchone()
            num_lines = row[0]
        else:
            num_lines = sum(1 for line in open(source)) - header_lines

        report_number.writerow([datetime.now(), source, zone, dim, num_lines])
    csvfile.close()




class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("./reporting/lastrun.log", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


def fbde(conn, cur):
    log_count("../données/Product", "Fichier", "Product", 0)
    log_count("../données/FobsCalls", "Fichier", "Calls", 0)
    log_count("../données/FobsPhones", "Fichier", "People", 0)
    log_count("../données/dpt_fr", "Fichier", "Location", 0)


    print("DROPPING SCHEMA fbde...", end="", flush=True)
    cur.execute("DROP SCHEMA IF EXISTS fbde CASCADE")
    cur.execute("CREATE SCHEMA fbde")
    conn.commit()
    print("DONE")
    print("CREATING TABLES IN fbde...", end="", flush=True)
    cur.execute(open("./e/CREATE_E.sql", "r").read())
    print("DONE")

    print("INSERTING DATA IN fbde.FobsCalls...", end="", flush=True)
    copy_sql = """
               COPY fbde.fobsCalls FROM stdin
               WITH delimiter ';' CSV QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/FobsCalls", "r"))
    print("DONE")

    print("INSERTING DATA IN fbde.FobsPhones...", end="", flush=True)
    copy_sql = """
               COPY fbde.FobsPhones FROM stdin
               WITH delimiter ';' CSV QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/FobsPhones", "r"))
    print("DONE")

    print("INSERTING DATA IN fbde.Product...", end="", flush=True)
    copy_sql = """
               COPY fbde.Product FROM stdin
               WITH delimiter ';' CSV QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/Product", "r"))
    print("DONE")

    print("INSERTING DATA IN fbde.dpt_fr...", end="", flush=True)
    copy_sql = """
               COPY fbde.dpt_fr FROM stdin
               WITH delimiter ';' CSV QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/dpt_fr", "r"))
    print("DONE")
    log_count("fbde.Product", "E", "Product", 0)
    log_count("fbde.FobsCalls", "E", "Calls", 0)
    log_count("fbde.FobsPhones", "E", "People", 0)
    log_count("fbde.dpt_fr", "E", "Location", 0)


def fbdt(conn, cur):
    print("DROPPING SCHEMA fbdt...", end="", flush=True)
    cur.execute("DROP SCHEMA IF EXISTS fbdt CASCADE")
    cur.execute("CREATE SCHEMA fbdt")
    conn.commit()
    print("DONE")
    print("CREATING TABLES IN fbdt...", end="", flush=True)
    cur.execute(open("./t/CREATE_T.sql", "r").read())
    print("DONE")
    print("INSERTING DATA IN TABLES IN fbdt...", end="", flush=True)
    cur.execute(open("./t/INSERT.sql", "r").read())
    log_count("fbdt.Product", "T", "Product", 0)
    log_count("fbdt.FobsCalls", "T", "Calls", 0)
    log_count("fbdt.FobsPhones", "T", "People", 0)
    log_count("fbdt.dpt_fr", "T", "Location", 0)
    print("DONE")
    print("TRANSFORMING DATA...", end="", flush=True)
    if len(sys.argv) == 1 or (len(sys.argv) > 1 and sys.argv[1] != "skip"):
        fbdt_transform_data(conn, cur)
        print("DONE")
    else:
        print("SKIPPED")
    print("CREATING VIEWS FOR DW...", end="", flush=True)
    cur.execute(open("./t/CREATE_VIEW.sql", "r").read())
    log_count("fbdt.dim_Product", "T Vues", "Product", 0)
    log_count("fbdt.fac_Calls", "T Vues", "Calls", 0)
    log_count("fbdt.dim_people", "T Vues", "People", 0)
    log_count("fbdt.dim_location", "T Vues", "Location", 0)
    print("DONE")


def fbdl(conn, cur):
    print("DROPPING SCHEMA fbdl...", end="", flush=True)
    cur.execute("DROP SCHEMA IF EXISTS fbdl CASCADE")
    cur.execute("CREATE SCHEMA fbdl")
    conn.commit()
    print("DONE")
    print("CREATING DATAWAREHOUSE IN fbdl FROM VIEWS IN fbdt...", end="", flush=True)
    cur.execute(open("./l/CREATE_L.sql", "r").read())
    log_count("fbdl.dim_Product", "L", "Product", 0)
    log_count("fbdl.Calls", "L", "Calls", 0)
    log_count("fbdl.dim_people", "L", "People", 0)
    log_count("fbdl.dim_location", "L", "Location", 0)
    print("DONE")
    print("CALCULATING BEST PRODUCTS")
    sql = "\
        Select P.row_num\
        from fbdl.dim_product P\
        LEFT JOIN fbdl.Calls C ON P.row_num = C.FK_Product\
        GROUP BY P.row_num\
        having sum(C.Duration) notnull\
        ORDER BY sum(C.Duration) DESC;\
        "
    cur.execute(sql)
    x = []
    for row in cur:
        x.append(row[0])
    nb_top_ten = int(len(x)/10)
    for product in x[0:nb_top_ten]:
        cur.execute(
            "UPDATE fbdl.dim_Product\
                SET BestProduct=True\
                WHERE row_num = {}".format(product)
        )
    conn.commit()
    print("DONE")

def reporting(conn, cur):
    print("CALCULATING REPORTING")
    with open("./reporting/lines.csv", "r") as csvfile:
        log = csv.reader(csvfile)
        # Peu élégant:
        zones = ('Fichier', 'E', 'T', 'T Vues', 'L')
        y_pos = np.arange(len(zones))
        Product = np.array([], dtype=np.integer)
        Calls = np.array([], dtype=np.integer)
        People = np.array([], dtype=np.integer)
        Location = np.array([], dtype=np.integer)
        for row in log:
            if row[3] == "Product":
                Product = np.append(Product, [int(row[4])])
            elif row[3] == "Calls":
                Calls = np.append(Calls, [int(row[4])])
            elif row[3] == "People":
                People = np.append(People, [int(row[4])])
            elif row[3] == "Location":
                Location = np.append(Location, [int(row[4])])

        plt.clf()
        plt.bar(y_pos, Product, align='center', alpha=0.5)
        plt.xticks(y_pos, zones)
        plt.ylim(0, np.amax(Product))
        plt.ylabel("Nombre de Lignes")
        plt.title("Nombre de ligne dans les différentes zone pour la dimension Produits")
        for i, v in enumerate(Product):
            plt.text(i-0.13,
            v/Product[i]+5,
            Product[i])
        plt.savefig('./reporting/Product.png')

        plt.clf()
        plt.bar(y_pos, Calls, align='center', alpha=0.5)
        plt.xticks(y_pos, zones)
        plt.ylim(0, np.amax(Calls))
        plt.ylabel("Nombre de Lignes")
        plt.title("Nombre de ligne dans les différentes zone pour la dimension Calls")
        for i, v in enumerate(Calls):
            plt.text(i-0.13,
            v/Calls[i]+5,
            Calls[i])
        plt.savefig('./reporting/Calls.png')

        plt.clf()
        plt.bar(y_pos, People, align='center', alpha=0.5)
        plt.xticks(y_pos, zones)
        plt.ylim(0, np.amax(People))
        plt.ylabel("Nombre de Lignes")
        plt.title("Nombre de ligne dans les différentes zone pour la dimension Produits")
        for i, v in enumerate(People):
            plt.text(i-0.13,
            v/People[i]+5,
            People[i])
        plt.savefig('./reporting/People.png')

        plt.clf()
        plt.bar(y_pos, Location, align='center', alpha=0.5)
        plt.xticks(y_pos, zones)
        plt.ylim(0, np.amax(Location))
        plt.ylabel("Nombre de Lignes")
        plt.title("Nombre de ligne dans les différentes zone pour la dimension Produits")
        for i, v in enumerate(Location):
            plt.text(i-0.13,
            v/Location[i]+5,
            Location[i])
        plt.savefig('./reporting/Location.png')


    # Pas eu le temps de faire de graphe et de mettre en forme cette partie
    print("[Lignes Importées correctement en zone E\tLignes en erreur]")
    with open("../données/FobsCalls", "r") as csvfile:
        source = csv.reader(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        cur.execute("Select * from fbde.FobsCalls")
        FobsCalls = np.array([0, 0], dtype=np.integer)
        for i, j in zip(source, cur):
            rvalue = compare(i,j)
            if rvalue is True:
                FobsCalls[0] += 1
            else:
                FobsCalls[1] += 1
        print("FobsCalls:",end="")
        print(FobsCalls)

    with open("../données/FobsPhones", "r") as csvfile:
        source = csv.reader(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        cur.execute("Select * from fbde.FobsPhones")
        Fobs = np.array([0, 0], dtype=np.integer)
        for i, j in zip(source, cur):
            rvalue = compare(i,j)
            if rvalue is True:
                Fobs[0] += 1
            else:
                Fobs[1] += 1
        print("FobsPhones:",end="")
        print(Fobs)

    with open("../données/dpt_fr", "r") as csvfile:
        source = csv.reader(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        cur.execute("Select * from fbde.dpt_fr")
        Fobs = np.array([0, 0], dtype=np.integer)
        for i, j in zip(source, cur):
            rvalue = compare(i,j)
            if rvalue is True:
                Fobs[0] += 1
            else:
                Fobs[1] += 1
        print("dpt_fr:",end="")
        print(Fobs)

    with open("../données/Product", "r") as csvfile:
        source = csv.reader(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        cur.execute("Select * from fbde.Product")
        Fobs = np.array([0, 0], dtype=np.integer)
        for i, j in zip(source, cur):
            rvalue = compare(i,j)
            if rvalue is True:
                Fobs[0] += 1
            else:
                Fobs[1] += 1
        print("Product:",end="")
        print(Fobs)




if __name__ == "__main__":
    # Reinit Log (touch file)
    a = open("./reporting/lines.csv", "w")
    a.close()
    a = open("./reporting/mod.csv", "w")
    a.close()
    #Process
    conn, cur = bdd()
    sys.stdout = Logger()
    fbde(conn, cur)
    fbdt(conn, cur)
    fbdl(conn, cur)
    reporting(conn, cur)
    print("PROCESS COMPLETE")
    cur.close()
    conn.close()
