# Présentations

1 thème dans une techno (gestion de données et datawarehouse)

## Mon sujet

Résolution de problème avec une BDD Graphe (Orient ou Neo4J) 
 * Rubik's Cube
 * Une application métier

## Date
 * V0 : t-1 semaine
 * V1 : t-2j (dimanche pour Mardi, mardi pour jeudi)

## Format (négociable) (1h30)
 * 10-15 min de présentation
 * 10-15 min sur un Hello World ()
 * 20 min -> Exercice 1 (facile)
 * 20+ min -> Exercice 2 (avancé)