BEGIN;
DROP SCHEMA IF EXISTS fbdl CASCADE;
CREATE SCHEMA fbdl;
COMMIT;

BEGIN;
CREATE TABLE fbdl.dim_Date AS
SELECT * from fbdt.dim_Date;

-- A voir si intéressant de prendre PK ou Unique et Index au besoin derrière

ALTER TABLE fbdl.dim_Date
  ADD CONSTRAINT dim_date_primary_key PRIMARY KEY (date)
  -- TODO: AJOUT DE TOUTES LES CONTRAINTES SUR LES ENTIERS
;

CREATE TABLE fbdl.dim_Livre AS
SELECT * FROM fbdt.dim_Livre;

ALTER TABLE fbdl.dim_Livre
  ADD CONSTRAINT dim_Livre_primary_key PRIMARY KEY (isbn)
  -- TODO: Les autres contraintes
;

CREATE TABLE fbdl.dim_Magasin AS
Select * from fbdt.dim_Magasin;

ALTER TABLE fbdl.dim_Magasin
  ADD CONSTRAINT dim_Magasin_primary_key PRIMARY KEY (magasin)
  -- TODO: Les autres contraintes
;

CREATE TABLE fbdl.ventes AS
Select * from fbdt.fac_ventes;

ALTER TABLE fbdl.Ventes
  ADD CONSTRAINT ventes_fk_Date FOREIGN KEY (FK_Date) REFERENCES fbdl.dim_Date(date)
  ,ADD CONSTRAINT ventes_fk_Livre FOREIGN KEY (FK_Livre) REFERENCES fbdl.dim_Livre(isbn)
  ,ADD CONSTRAINT ventes_fk_Magasin FOREIGN KEY (FK_Magasin) REFERENCES fbdl.dim_Magasin(magasin)
;

COMMIT;
