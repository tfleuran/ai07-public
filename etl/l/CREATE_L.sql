BEGIN;
CREATE TABLE fbdl.dim_Date AS
SELECT * from fbdt.dim_Date;

-- A voir si intéressant de prendre PK ou Unique et Index au besoin derrière

CREATE TABLE fbdl.dim_Livre AS
SELECT * FROM fbdt.dim_Livre;

CREATE TABLE fbdl.dim_Magasin AS
Select * from fbdt.dim_Magasin;

CREATE TABLE fbdl.ventes AS
Select * from fbdt.fac_ventes;

----------------------------------------------
-- Contraintes


ALTER TABLE fbdl.dim_Date
  ALTER COLUMN date TYPE date,
  ALTER COLUMN JourSemaine TYPE INTEGER,
  ALTER COLUMN JOUR TYPE INTEGER,
  ALTER COLUMN Semaine TYPE INTEGER,
  ALTER COLUMN Mois TYPE INTEGER,
  ALTER COLUMN TRIMESTRE TYPE INTEGER,
  ALTER COLUMN Semestre TYPE INTEGER,
  ALTER COLUMN Annee TYPE INTEGER,
  ADD CONSTRAINT dim_date_primary_key PRIMARY KEY (row_num),
  ADD CONSTRAINT dim_date_JourSemaine CHECK (JourSemaine >= 1 and JourSemaine <= 7),
  ADD CONSTRAINT dim_date_Semaine CHECK (Semaine >= 1 and Semaine <= 53),
  ADD CONSTRAINT dim_date_Mois CHECK (Mois >= 1 and Mois <= 12),
  ADD CONSTRAINT dim_date_Trimestre CHECK (Trimestre >= 1 and Trimestre <= 4),
  ADD CONSTRAINT dim_date_Semestre CHECK (Semestre >= 1 and Semestre <= 2),
  ADD CONSTRAINT dim_date_Annee CHECK (Annee >= 1)
;

ALTER TABLE fbdl.dim_Livre
  ADD CONSTRAINT dim_Livre_primary_key PRIMARY KEY (row_num)
  -- TODO: Les autres contraintes
;

ALTER TABLE fbdl.dim_Magasin
  ADD CONSTRAINT dim_Magasin_primary_key PRIMARY KEY (row_num)
  -- TODO: Les autres contraintes
;

ALTER TABLE fbdl.Ventes
  ADD CONSTRAINT ventes_fk_Date FOREIGN KEY (FK_Date) REFERENCES fbdl.dim_Date(row_num),
  ADD CONSTRAINT ventes_fk_Livre FOREIGN KEY (FK_Livre) REFERENCES fbdl.dim_Livre(row_num),
  ADD CONSTRAINT ventes_fk_Magasin FOREIGN KEY (FK_Magasin) REFERENCES fbdl.dim_Magasin(row_num)
;

COMMIT;