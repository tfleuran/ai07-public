set search_path to fbdl;

Select D.JourSemaine
  ,sum(V.quantite)
  ,ROUND(sum(V.quantite)/(Select sum(quantite) sub1 FROM ventes)*100) PART
FROM ventes V
LEFT JOIN dim_date D ON V.fk_date = D.date
GROUP BY D.JourSemaine
ORDER BY D.JourSemaine
;

Select D.JourSemaine
  ,ROUND(sum(V.quantite)/S.sub1) Moyenne_Vente
FROM ventes V
LEFT JOIN dim_date D ON V.fk_date = D.date
LEFT JOIN (
  Select JourSemaine, count(*) sub1
  from dim_date
  group by JourSemaine
) S ON D.JourSemaine = S.JourSemaine
WHERE D.JourSemaine != 6
GROUP BY D.JourSemaine, S.sub1
ORDER BY D.JourSemaine
;
