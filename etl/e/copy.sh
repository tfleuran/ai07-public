#!/bin/sh
echo 'Amenagement de la base: Recréation des tables'
psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -f ~/Documents/AI07/ai07/etl/e/Extract.sql

echo 'Import des Données Catalogue, 1443 lignes attendues'
psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.catalogue(ref, isbn, title, authors, language, pubdate, publisher, tags, genre) FROM '~/Documents/AI07/ai07/données/Catalogue.csv' delimiter E'\t' CSV HEADER QUOTE '\"';"
echo 'Import des Données Insee, 95 lignes attendues'
psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.insee FROM '~/Documents/AI07/ai07/données/departementsInsee2003.txt' delimiter ';' CSV QUOTE '\"';"
echo 'Import des Donnéees Fantastic, 512018 lignes attendues'
psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.ventes FROM '~/Documents/AI07/ai07/données/Fantastic' delimiter ';' CSV QUOTE '\"';"
echo 'Import des Donnéees Marketing, 152 lignes attendues'
psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.marketing FROM '~/Documents/AI07/ai07/données/marketing.csv' delimiter ',' CSV HEADER QUOTE '\"';"
