-- CLEAN-UP
BEGIN;
SET search_path TO fbde;
DROP TABLE IF EXISTS fbde.Ventes;
DROP TABLE IF EXISTS fbde.Marketing;
DROP TABLE IF EXISTS fbde.Insee;
DROP TABLE IF EXISTS fbde.Catalogue;
COMMIT;

-- CREATION DES TABLES
BEGIN;
SET search_path TO fbde;
CREATE TABLE IF NOT EXISTS fbde.Ventes(
	ticketNumber TEXT,
	date TEXT,
	iSBN TEXT,
	store TEXT
);

CREATE TABLE IF NOT EXISTS fbde.Marketing(
	magasin Text,
	departement Text,
	rayonnageCode Text,
	rayonnageLabel Text,
	RayonBS Text,
	RayonR Text
);

CREATE TABLE IF NOT EXISTS fbde.Insee(
	departementCode Text,
	departementLabel Text,
	population Text
);

CREATE TABLE IF NOT EXISTS fbde.Catalogue(
	ref Text,
	iSBN Text,
	title Text,
	authors Text,
	language Text,
	pubdate Text,
	publisher Text,
	tags Text,
	genre Text
);

COMMIT;

-- BEGIN;
-- SET search_path TO fbde;
