BEGIN;
SET Search_path to fbdl;

CREATE OR REPLACE VIEW  ventes_par_JourSemaine AS
select 
	JourSemaine
	,Count(*)
from ventes
left join dim_date on fk_date = row_num
group by JourSemaine
order by JourSemaine;

CREATE OR REPLACE VIEW  Moyenne_ventes_hors_samedi as 
select
	count(*)/(select count(*) from dim_date where jourSemaine != 6 and jourSemaine is not null)
from ventes
left join dim_date on fk_date = row_num
where jourSemaine != 6 and jourSemaine is not null
;

CREATE OR REPLACE VIEW  Ratio_ventes_samedi_agains_moyenne as
select 
	--D.JourSemaine
	--,Count(*)
	SUB2.COL1/SUB1.COL1
from ventes
left join dim_date D on fk_date = D.row_num
left join ( -- Calcul de la moyenne des ventes pour un jour hors samedi et données null
	select
		count(*)::decimal/(select count(*) from dim_date where jourSemaine != 6) COL1
	from ventes
	left join dim_date on fk_date = row_num
	where jourSemaine != 6
) SUB1 on 1=1
left join ( -- Calcul de la moyenne des ventes par jour de la semaine
	select
		B.joursemaine,
		count(*)::decimal/COLA COL1
	from ventes
	left join dim_date B on fk_date = B.row_num
	left join (
		select joursemaine, count(*) COLA
		from dim_date
		group by joursemaine
	) A on A.joursemaine = B.joursemaine
	group by B.joursemaine, A.COLA
) SUB2 on D.joursemaine = SUB2.joursemaine
where D.joursemaine = 6
group by D.JourSemaine, SUB1.COL1, SUB2.COL1
order by D.JourSemaine;

----------------------------------
CREATE OR REPLACE VIEW Ventes_Against_Rayonnage AS 
select 
	M.rayonnagecode,
	count(*),
	SUB1.COL1,
	count(*)/SUB1.COL1
from ventes V
left join dim_magasin M on V.fk_magasin = M.row_num
left join ( -- Nombre de magasins par rayonnage
	select rayonnagecode, count(*) COL1
	from dim_magasin
	group by rayonnagecode
) SUB1 on M.rayonnagecode = SUB1.rayonnagecode
group by M.rayonnagecode, SUB1.COL1
;

CREATE OR REPLACE VIEW iso_dim_magasin_rayonnage AS 
select
	row_num,
	magasin,
	departementcode,
	departementlabel,
	departementpopulation,
	rayonBS,
	RayonR
FROM dim_magasin
;

--------------------------------------------------

CREATE VIEW moyenne_vente_livres AS 
select 
	count(*)::decimal/(select count(*) from dim_livre)
from Ventes V
;

-- Best-seller : 10% des meilleurs ventes
create view dim_livre_bs as
select 
	L.*,
	case 
		when BS.row_num is not null then true
		else false
	end bs
from dim_livre L
left outer join (
	select -- Délimitation des BS 
		*
	from (
		select 
			row_number() over() RANK,
			row_num,
			NB
		from (select -- Classment des ventes
				l.row_num row_num,
				count(*) NB
			from ventes V
			left join dim_livre L on V.fk_livre = L.row_num 
			where L.row_num is not null
			group by L.row_num
			order by count(*) desc) list
	) ranking
	left join (select count(*) size_list from dim_livre) s_l on 1=1
	where ranking.rank <= s_l.size_list* 10/100
) BS on BS.row_num = L.row_num
order by L.row_num
;


COMMIT;