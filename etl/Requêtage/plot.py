import matplotlib.pyplot as plt
import psycopg2
import sys


def bdd():
    try:
        conn = psycopg2.connect(
            host="tuxa.sme.utc",
            dbname="dbbdd1p006",
            user="bdd1p006",
            password="NOP",
        )
        cur = conn.cursor()
        return conn, cur
    except:
        print("Connexion impossible")
        sys.exit()


if __name__ == "__main__":
    conn, cur = bdd()
    sql = """
          select
	        D.date,
	        count(*)
          from fbdl.ventes V
          left join fbdl.dim_date D on V.fk_date = D.row_num
          where D.date is not null
          group by D.Date
          order by D.Date;
          ;
          """
    cur.execute(sql)
    x = []
    y = []
    for row in cur:
        x.append(row[0])
        y.append(row[1])
    plt.plot(x, y)
    plt.show()    
    cur.close()
    conn.close()
