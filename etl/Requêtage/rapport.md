# Étude des ventes en fonction du jour de la semaine

L'étude des ventes en fonction du jour de la semaine nous donne le résultat suivant:

| joursemaine | count  |
| ----------- | ------ |
|           2 |  78912 |
|           3 |  78594 |
|           4 |  81319 |
|           5 |  78693 |
|           6 | 174766 |
|             |  19734 |

On peut ici remarquer de fortes ventes le samedi.

Il est intéressant d'étudier la moyenne des ventes des jours hors samedi. Nous obtenons:
```
1519
```
Etudions maintenant le ratio des ventes parr jour de la semaine par rapport à la moyenne des ventes hors samedi:

| joursemaine  | count  |        Ratio       
| -------------|--------|------------------------
|            2 |  78912 | 0.99888994785032175042
|            3 |  78594 | 0.99486461579161835528
|            4 |  81319 |     1.0099365633943558
|            5 |  78693 | 0.99611778520612035562
|            6 | 174766 |     2.2122364231803697
|              |  19734 |                       

On peut donc voir une forte hausse des ventes le samedi par rapport à la moyenne des ventes des autres jours.
Aucune explication disponible pour ce phénomène.

# Ventes en fonction de l'organisation du rayonnage

Nous cherchons ici la quantité de ventes par organisation de rayonnage, en rapportant cette part au nombre de magasins avec cette organisation:
|rayonnagecode | count  | nbMagasins | Ventes/rayonnagecode/magasin 
|---------------|--------:|------:|----------:
|               |  16823 |      |         
| E             |  24274 |   12 |     2022
| A             | 390219 |  112 |     3484
| Y             |  80702 |   28 |     2882

On peut remarquer qu'il y a plus de ventes lorsque les rayonnages sont organisés par auteur.

# Ventes de livres

On peut observer que le nombre moyen de ventes pour un livre est de:
```
354.8288288288288288
```

Si observe la répartition temporelle des ventes de livres, ou les fortes ventes du samedi, on peut voir une hausse des ventes en Décembre:

![Graphe des ventes en fonction du temps](Figure_1.png)

# Best Seller

On peut définir un best-seller comme un des livres appartenant aux 10% des livres ayant fait le plus de ventes.

La liste des livres et leur indication en tant que Best-Seller est disponible dans la vue *dim_livre_bs*.