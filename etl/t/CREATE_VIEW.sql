BEGIN;

SET search_path TO fbdt;

CREATE VIEW dim_Magasin AS
SELECT
  M.row_num
  ,M.magasin AS MAGASIN
  ,I.departementcode AS departementcode
  ,I.departementLabel AS departementLabel
  ,I.population AS departementPopulation
  ,M.rayonnageCode AS rayonnageCode
  ,M.rayonnageLabel AS rayonnageLabel
  ,M.RayonBS AS RayonBS
  ,M.RayonR AS RayonR
FROM marketing M
LEFT JOIN Insee I ON M.departement = I.departementcode
;

CREATE VIEW dim_Livre AS
SELECT
  C.row_num
  ,C.ISBN
  ,C.title AS Titre
  ,C.authors AS Auteurs
  ,C.language AS Langue
  ,C.publisher AS Editeur
  ,C.pubdate AS DatePublication
  ,EXTRACT(YEAR FROM C.pubdate) AS AnneePublication
  ,C.Genre
  ,P.Price AS Prix
  ,S.ForSales
  ,NULL AS BestSeller
FROM CATALOGUE C
LEFT JOIN Prices P ON P.ISBN = C.ISBN
LEFT JOIN ForSales S ON S.ISBN = C.ISBN;

CREATE VIEW dim_Date AS
Select
  row_number() over() AS row_num
  ,Date AS DATE
  ,EXTRACT(isodow FROM date) AS JourSemaine
  ,EXTRACT(day FROM date) AS Jour
  ,EXTRACT(week FROM date) AS Semaine
  ,EXTRACT(month FROM date) AS Mois
  ,EXTRACT(Quarter FROM date) AS Trimestre
  ,CASE
    WHEN EXTRACT(Quarter FROM date) = 1  OR EXTRACT(Quarter FROM date) = 2
      THEN 1
    WHEN EXTRACT(Quarter FROM date) = 3  OR EXTRACT(Quarter FROM date) = 4
      THEN 2
  END AS Semestre
  ,EXTRACT(Year FROM date) AS Annee
FROM ventes
WHERE date is not NULL
GROUP BY date
;

CREATE VIEW fac_ventes AS
Select
  --V.row_num
  V.ticketNumber as Num_ticket
  ,Count(*) AS Quantite
  ,Count(*) * L.Prix AS CA
  ,COALESCE(L.row_num, NULL) AS FK_Livre
  ,COALESCE(M.row_num, NULL) AS FK_magasin
  ,COALESCE(D.row_num, NULL) AS FK_Date
FROM ventes V
LEFT JOIN dim_Date D ON D.date = V.date
LEFT JOIN dim_Livre L ON L.ISBN = V.ISBN
LEFT JOIN dim_Magasin M ON M.magasin = V.store
group by 
  V.ticketNumber
  ,L.row_num
  ,M.row_num
  ,D.row_num
  ,L.Prix
;

COMMIT;