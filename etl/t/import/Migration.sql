BEGIN;
DROP SCHEMA IF EXISTS fbdt CASCADE;
CREATE SCHEMA fbdt;
COMMIT;

BEGIN;
SET search_path TO fbdt;
CREATE TABLE ext_VENTES(
  ticketnumber BIGINT,
  date Date,
  isbn BIGINT,
  store TEXT
);

CREATE TABLE ext_CATALOGUE(
 ref INTEGER,
 isbn BIGINT,
 title TEXT,
 authors TEXT,
 language TEXT,
 pubdate Date,
 publisher TEXT,
 tags TEXT,
 genre TEXT
);

CREATE TABLE ext_Insee(
  departementcode INTEGER,
  departementLabel TEXT,
  population INTEGER
);

CREATE TABLE ext_Marketing(
  magasin Text,
	departement INTEGER,
	rayonnageCode Text,
	rayonnageLabel Text,
	RayonBS BOOLEAN,
	RayonR BOOLEAN
);
COMMIT;

-- INSERT
BEGIN;
INSERT INTO fbdt.ext_VENTES
SELECT
  CASE WHEN ticketnumber SIMILAR TO '\d*'
    THEN CAST(ticketnumber AS BIGINT)
    ELSE NULL
  END
  ,CASE WHEN date SIMILAR TO '\d{4}-\d{2}-\d{2}'
    THEN TO_DATE(date, 'YYYY-MM-DD')
    ELSE NULL
  END
  ,CASE WHEN isbn SIMILAR TO '\d*'
    THEN CAST(isbn AS BIGINT)
    ELSE NULL
  END
  ,store
FROM fbde.ventes;

INSERT INTO fbdt.ext_CATALOGUE
SELECT
  CAST(ref as INTEGER)
  ,CAST(isbn AS BIGINT)
  ,title
  ,authors
  ,language
  ,TO_DATE(pubdate, 'YYYY-MM-DD')
  ,publisher
  ,tags
  ,genre
FROM fbde.CATALOGUE;

INSERT INTO fbdt.ext_Insee
SELECT
  CAST(departementcode AS INTEGER)
  ,departementLabel
  ,CAST(population AS INTEGER)
FROM fbde.Insee;

INSERT INTO fbdt.ext_Marketing
SELECT
  magasin
  ,CAST(departement as INTEGER)
  ,rayonnageCode
  ,rayonnageLabel
  ,RayonBS::BOOLEAN
  ,RayonR::BOOLEAN
FROM fbde.Marketing;

-- Pour l'instant, pas de contrainte supplémentaire que le type

-- AJOUT d'INDEX


-- ADD CONSTRAINTS
-- Ventes
-- ALTER TABLE fbdt.Ventes
-- ADD CONSTRAINT date CHECK(
--   date IS NOT NULL
-- ),
-- ADD CONSTRAINT ISBN CHECK(
--   ISBN IS NOT NULL
-- ),
-- ADD CONSTRAINT Store CHECK(
--   store IS NOT NULL
--   AND store SIMILAR TO 'M\d{1-3}'
-- );

-- ALTER TABLE fbdt.Marketing ADD CONSTRAINT rayonnageCode CHECK (
--   (rayonnageCode like 'A'
--   OR rayonnageCode like 'E'
--   OR rayonnageCode like 'Y')
--   AND rayonnageCode IS NOT NULL
-- );

COMMIT;

-- Tri des données
BEGIN;
-- Creation des tables "clean"  (vues sur gros volume de données?)
SET search_path TO fbdt;

CREATE Table cle_Catalogue AS
SELECT * from ext_CATALOGUE
WHERE 1=1
AND isbn is not NULL
--AND (char_length(isbn) = 10 OR char_length(isbn) = 13);
;

CREATE Table cle_Insee AS
Select * from ext_Insee
WHERE 1=1
AND departementcode is NULL;

CREATE Table cle_marketing AS
Select * from ext_Marketing
WHERE 1=1
AND magasin is not NULL
AND departement is not NULL
AND departement in (select departementcode from ext_Insee);

CREATE Table cle_ventes AS
SELECT v.* from ext_ventes v
LEFT OUTER JOIN cle_marketing m ON v.store = m.magasin
LEFT OUTER JOIN cle_CATALOGUE c ON v.isbn = c.isbn
WHERE 1=1
AND date is not NULL
AND v.isbn is not NULL
AND v.store is not NULL
AND m.magasin is not null
AND c.isbn is not NULL;

-- creattion des tables de récupération

CREATE TABLE oth_Catalogue AS
SELECT * from ext_CATALOGUE
WHERE not (1=1
AND isbn is not NULL
-- AND (isbn >= 100 AND char_length(isbn) = 13)
);

CREATE TABLE oth_Insee AS
Select * from ext_Insee
WHERE not (1=1
AND departementcode is NULL);

CREATE Table oth_marketing AS
Select * from ext_Marketing
WHERE not (1=1
AND magasin is not NULL
AND departement is not NULL
AND departement in (select departementcode from ext_Insee));

CREATE TABLE oth_ventes AS
SELECT v.* from ext_ventes v
LEFT OUTER JOIN oth_marketing m ON v.store = m.magasin
LEFT OUTER JOIN oth_CATALOGUE c ON v.isbn = c.isbn
WHERE not (1=1
AND date is not NULL
AND v.isbn is not NULL
AND v.store is not NULL
AND m.magasin is not null
AND c.isbn is not NULL);

COMMIT;

-- Transformations
BEGIN;
Set search_path to fbdt;

CREATE VIEW dim_Magasin AS
SELECT
  M.magasin
  ,I.departementcode
  ,I.departementLabel
  ,I.population
  ,M.rayonnageCode
  ,M.rayonnageLabel
  ,M.RayonBS
  ,M.RayonR
FROM cle_marketing M
LEFT JOIN cle_Insee I ON M.departement = I.departementcode
;

CREATE View dim_Livre AS
SELECT
  ISBN
  ,title AS Titre
  ,authors AS Auteurs
  ,language AS Langue
  ,publisher AS Editeur
  ,pubdate AS DatePublication
  ,EXTRACT(YEAR FROM pubdate)
  ,Genre
FROM cle_CATALOGUE;

CREATE View dim_Date AS
Select
  Date AS DATE
  ,EXTRACT(isodow FROM date) AS JourSemaine
  ,EXTRACT(day FROM date) AS Jour
  ,EXTRACT(week FROM date) AS Semaine
  ,EXTRACT(month FROM date) AS Mois
  ,EXTRACT(Quarter FROM date) AS Trimestre
  ,CASE
    WHEN EXTRACT(Quarter FROM date) = 1  OR EXTRACT(Quarter FROM date) = 2
      THEN 1
    WHEN EXTRACT(Quarter FROM date) = 3  OR EXTRACT(Quarter FROM date) = 4
      THEN 2
  END AS Semestre
  ,EXTRACT(Year FROM date) AS Annee
FROM cle_ventes
GROUP BY DAte; -- Réduction du volume

CREATE VIEW fac_ventes AS
Select
  COUNT(*) AS Quantite
  ,ISBN AS FK_Livre
  ,store AS FK_magasin
  ,date AS FK_Date
FROM cle_ventes
GROUP BY ISBN
    ,store
    ,date
;

COMMIT;
