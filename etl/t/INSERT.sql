BEGIN;
INSERT INTO fbdt.VENTES
    SELECT
    --row_number() over()
    CASE WHEN ticketnumber SIMILAR TO '\d*'
        THEN CAST(ticketnumber AS BIGINT)
        ELSE NULL
    END
    ,CASE WHEN date SIMILAR TO '\d{4}-\d{2}-\d{2}'
        THEN TO_DATE(date, 'YYYY-MM-DD')
        ELSE NULL
    END
    ,CASE WHEN isbn SIMILAR TO '\d*'
        THEN CAST(isbn AS BIGINT)
        ELSE NULL
    END
    ,store
    FROM fbde.ventes
;

INSERT INTO fbdt.CATALOGUE
    SELECT
    row_number() over()
    ,CAST(ref as INTEGER)
    ,CAST(isbn AS BIGINT)
    ,title
    ,authors
    ,language
    ,TO_DATE(pubdate, 'YYYY-MM-DD')
    ,publisher
    ,tags
    ,genre
    FROM fbde.CATALOGUE
;

INSERT INTO fbdt.Insee
    SELECT
    CAST(departementcode AS INTEGER)
    ,departementLabel
    ,CAST(population AS INTEGER)
    FROM fbde.Insee
;

INSERT INTO fbdt.Marketing
    SELECT
    row_number() over()
    ,magasin
    ,CAST(departement as INTEGER)
    ,rayonnageCode
    ,rayonnageLabel
    ,RayonBS::BOOLEAN
    ,RayonR::BOOLEAN
    FROM fbde.Marketing
;

INSERT INTO fbdt.Prices
    SELECT
    CAST(isbn AS BIGINT)
    ,CAST(price AS INTEGER)
    FROM fbde.Prices
;

INSERT INTO fbdt.ForSales
    SELECT
    CAST(isbn AS BIGINT)
    ,ForSales::BOOLEAN
    FROM fbde.ForSales
;
COMMIT;