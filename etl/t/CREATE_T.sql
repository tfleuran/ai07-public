BEGIN;
SET search_path TO fbdt;
CREATE TABLE VENTES(
    ticketnumber BIGINT,
    date Date,
    isbn BIGINT,
    store TEXT,
    data_check CHAR(5)
);

CREATE TABLE CATALOGUE(
    row_num INTEGER,
    ref INTEGER,
    isbn BIGINT,
    title TEXT,
    authors TEXT,
    language TEXT,
    pubdate Date,
    publisher TEXT,
    tags TEXT,
    genre TEXT,
    data_check CHAR(10)
);

CREATE TABLE Insee(
    departementcode INTEGER,
    departementLabel TEXT,
    population INTEGER,
    data_check CHAR(4)
);

CREATE TABLE Marketing(
    row_num INTEGER,
    magasin Text,
    departement INTEGER,
    rayonnageCode Text,
    rayonnageLabel Text,
    RayonBS BOOLEAN,
    RayonR BOOLEAN,
    data_check CHAR(7)
);

CREATE TABLE Prices(
    ISBN BIGINT,
    Price INTEGER,
    data_check CHAR(2)
);

CREATE TABLE ForSales(
    ISBN BIGINT,
    ForSales BOOLEAN,
    data_check CHAR(2)
);
COMMIT;