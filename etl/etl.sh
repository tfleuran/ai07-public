echo 'Amenagement de la base: Recréation des tables'
psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -f ~/Documents/AI07/ai07/etl/e/Extract.sql

# Format de données
echo "Source;Destination;nb_lignes_sources;nb_lignes_destination;Etat" > ~/Documents/AI07/ai07/etl/reporting/rapport.csv

# IMPORT
echo 'Import des données'
NB_BEFORE="$(wc -l ~/Documents/AI07/ai07/données/Catalogue.csv | awk '{print $1;}')"
let "NB_BEFORE -= 1" # Headers dans le fichier
echo "Import des Données Catalogue"
NB_AFTER=$(psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.catalogue(ref, isbn, title, authors, language, pubdate, publisher, tags, genre) FROM '~/Documents/AI07/ai07/données/Catalogue.csv' delimiter E'\t' CSV HEADER QUOTE '\"';" | awk '{print $2;}')
echo "${NB_AFTER}/${NB_BEFORE}"
echo "catalogue;bfde.catalogue;${NB_BEFORE};${NB_AFTER};" >> ~/Documents/AI07/ai07/etl/reporting/rapport.csv

NB_BEFORE="$(wc -l ~/Documents/AI07/ai07/données/departementsInsee2003.txt | awk '{print $1;}')"
echo "Import des Données Insee"
NB_AFTER=$(psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.insee FROM '~/Documents/AI07/ai07/données/departementsInsee2003.txt' delimiter ';' CSV QUOTE '\"';" | awk '{print $2;}')
echo "${NB_AFTER}/${NB_BEFORE}"
echo "insee;bfde.insee;${NB_BEFORE};${NB_AFTER};" >> ~/Documents/AI07/ai07/etl/reporting/rapport.csv

NB_BEFORE="$(wc -l ~/Documents/AI07/ai07/données/Fantastic | awk '{print $1;}')"
echo "Import des Données Fantastic"
NB_AFTER=$(psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.ventes FROM '~/Documents/AI07/ai07/données/Fantastic' delimiter ';' CSV QUOTE '\"';" | awk '{print $2;}')
echo "${NB_AFTER}/${NB_BEFORE}"
echo "fantastic;bfde.ventes;${NB_BEFORE};${NB_AFTER};" >> ~/Documents/AI07/ai07/etl/reporting/rapport.csv

NB_BEFORE="$(wc -l ~/Documents/AI07/ai07/données/marketing.csv | awk '{print $1;}')"
let "NB_BEFORE -= 1" # Headers dans le fichier
echo "Import des Données Marketing"
NB_AFTER=$(psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -c "\copy fbde.marketing FROM '~/Documents/AI07/ai07/données/marketing.csv' delimiter ',' CSV HEADER QUOTE '\"';" | awk '{print $2;}')
echo "${NB_AFTER}/${NB_BEFORE}"
echo "marketing;bfde.marketing;${NB_BEFORE};${NB_AFTER};" >> ~/Documents/AI07/ai07/etl/reporting/rapport.csv

# TRANSFERT VERS LA ZONE T
psql "postgresql://bdd1p006:NOP@tuxa.sme.utc/dbbdd1p006" -f ~/Documents/AI07/ai07/etl/t/import/Migration.sql
