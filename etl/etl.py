import psycopg2
import sys
import re
from datetime import datetime
from fbdt import fbdt_create_tables, fbdt_import_data, fbdt_transform_data


def bdd():
    try:
        conn = psycopg2.connect(
            host="tuxa.sme.utc",
            dbname="dbbdd1p006",
            user="bdd1p006",
            password="NOP",
        )
        cur = conn.cursor()
        return conn, cur
    except:
        print("Connexion impossible")
        sys.exit()


def log_count(source, dest, header_lines=0):
    report_number = open("./reporting/lines.csv", "a")
    if re.fullmatch("fbd.*", source) is not None:
        sql = "Select Count(*) from {};".format(source)
        cur.execute(sql)
        row = cur.fetchone()
        num_lines = row[0]
    else:
        num_lines = sum(1 for line in open(source)) - header_lines
    
    sql = "Select Count(*) from {};".format(dest)
    cur.execute(sql)
    row = cur.fetchone()
    integ = True if num_lines == row[0] else False
    report_number.write(
        "{};{};{};{};{};{}\n".format(
            datetime.now(), source, dest,num_lines, row[0], integ
        )
    )


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("lastrun.log", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


def fbde(conn, cur):

    print("DROPPING SCHEMA fbde...", end="", flush=True)
    cur.execute("DROP SCHEMA IF EXISTS fbde CASCADE")
    cur.execute("CREATE SCHEMA fbde")
    conn.commit()
    print("DONE")
    print("CREATING TABLES IN fbde...", end="", flush=True)
    cur.execute(open("./e/CREATE_E.sql", "r").read())
    print("DONE")


    print("INSERTING DATA IN fbde.Catalogue...", end="", flush=True)
    copy_sql = """
               COPY fbde.catalogue FROM stdin
               WITH delimiter E'\t' CSV HEADER QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/Catalogue.csv", "r"))
    log_count("../données/Catalogue.csv","fbde.catalogue",1)
    print("DONE")


    print("INSERTING DATA IN fbde.Insee...", end="", flush=True)
    copy_sql = """
               COPY fbde.insee FROM stdin
               WITH delimiter ';' CSV QUOTE '\"';
               """
    cur.copy_expert(
        sql=copy_sql, file=open("../données/departementsInsee2003.txt", "r")
    )
    log_count("../données/departementsInsee2003.txt","fbde.insee",0)
    print("DONE")
    
    
    print("INSERTING DATA IN fbde.ventes...", end="", flush=True)
    copy_sql = """
               COPY fbde.ventes FROM stdin
               WITH delimiter ';' CSV QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/Fantastic", "r"))
    log_count("../données/Fantastic","fbde.ventes",0)
    print("DONE")

    
    print("INSERTING DATA IN fbde.marketing...", end="", flush=True)
    copy_sql = """
               COPY fbde.marketing FROM stdin
               WITH delimiter ',' CSV HEADER QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/marketing.csv", "r"))
    log_count("../données/marketing.csv","fbde.marketing",1)
    print("DONE")

    print("INSERTING DATA IN fbde.prices...", end="", flush=True)
    copy_sql = """
               COPY fbde.prices FROM stdin
               WITH delimiter ';' CSV HEADER QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/Prices2015.csv", "r"))
    log_count("../données/Prices2015.csv","fbde.prices",1)
    print("DONE")

    print("INSERTING DATA IN fbde.forsales...", end="", flush=True)
    copy_sql = """
               COPY fbde.forsales FROM stdin
               WITH delimiter ';' CSV HEADER QUOTE '\"';
               """
    cur.copy_expert(sql=copy_sql, file=open("../données/Sales2015.csv", "r"))
    log_count("../données/Sales2015.csv","fbde.marketing",1)
    print("DONE")


def fbdt(conn, cur):
    print("DROPPING SCHEMA fbdt...", end="", flush=True)
    cur.execute("DROP SCHEMA IF EXISTS fbdt CASCADE")
    cur.execute("CREATE SCHEMA fbdt")
    conn.commit()
    print("DONE")
    print("CREATING TABLES IN fbdt...", end="", flush=True)
    cur.execute(open("./t/CREATE_T.sql", "r").read())
    print("DONE")
    print("INSERTING DATA IN TABLES IN fbdt...", end="", flush=True)
    cur.execute(open("./t/INSERT.sql", "r").read())
    log_count("fbde.catalogue","fbdt.catalogue")
    log_count("fbde.marketing","fbdt.marketing")
    log_count("fbde.insee","fbdt.insee")
    log_count("fbde.ventes","fbdt.ventes")
    log_count("fbde.prices","fbdt.prices")
    log_count("fbde.forsales","fbdt.forsales")
    print("DONE")
    print("TRANSFORMING DATA...", end="", flush=True)
    if len(sys.argv) == 1 or (len(sys.argv) > 1 and sys.argv[1] != "skip"):
        fbdt_transform_data(conn, cur)
        print("DONE")
    else:
        print("SKIPPED")
    print("CREATING VIEWS FOR DW...", end="", flush=True)
    cur.execute(open("./t/CREATE_VIEW.sql", "r").read())
    print("DONE")


def fbdl(conn, cur):
    print("DROPPING SCHEMA fbdl...", end="", flush=True)
    cur.execute("DROP SCHEMA IF EXISTS fbdl CASCADE")
    cur.execute("CREATE SCHEMA fbdl")
    conn.commit()
    print("DONE")
    print("CREATING DATAWAREHOUSE IN fbdl FROM VIEWS IN fbdt...", end="", flush=True)
    cur.execute(open("./l/CREATE_L.sql", "r").read())
    log_count("fbdt.ventes", "fbdl.ventes")
    log_count("fbdt.marketing", "fbdl.dim_magasin")
    log_count("fbdt.catalogue", "fbdl.dim_livre")
    print("DONE")


if __name__ == "__main__":
    conn, cur = bdd()
    sys.stdout = Logger()
    fbde(conn, cur)
    fbdt(conn, cur)
    fbdl(conn, cur)
    print("PROCESS COMPLETE")
    cur.close()
    conn.close()
