
import psycopg2
import sys
import re
from datetime import datetime


def log_count_trans(cur, num_lines, dest):
    report_number = open("./reporting/lines.csv", "a")

    sql = "Select Count(*) from {};".format(dest)
    cur.execute(sql)
    row = cur.fetchone()
    integ = True if num_lines == row[0] else False
    report_number.write(
        "{};{};{};{};{};{}\n".format(
            datetime.now(), dest, dest,num_lines, row[0], integ
        )
    )

def fbdt_create_tables(conn, cur):
    cur.execute("SET search_path TO fbdt")
    cur.execute(
        "CREATE TABLE VENTES(\
                row_num INTEGER,\
                ticketnumber BIGINT,\
                date Date,\
                isbn BIGINT,\
                store TEXT,\
                data_check CHAR(5)\
                )"
    )
    cur.execute(
        "CREATE TABLE CATALOGUE(\
                row_num INTEGER,\
                ref INTEGER,\
                isbn BIGINT,\
                title TEXT,\
                authors TEXT,\
                language TEXT,\
                pubdate Date,\
                publisher TEXT,\
                tags TEXT,\
                genre TEXT,\
                data_check CHAR(10)\
                )"
    )

    cur.execute(
        "CREATE TABLE Insee(\
                departementcode INTEGER,\
                departementLabel TEXT,\
                population INTEGER,\
                data_check CHAR(4)\
                )"
    )

    cur.execute(
        "CREATE TABLE Marketing(\
                row_num INTEGER,\
                magasin Text,\
    	        departement INTEGER,\
    	        rayonnageCode Text,\
    	        rayonnageLabel Text,\
    	        RayonBS BOOLEAN,\
    	        RayonR BOOLEAN,\
                data_check CHAR(7)\
                )"
    )
    conn.commit()


def fbdt_import_data(conn, cur):
    cur.execute(
        "INSERT INTO fbdt.VENTES\
        SELECT\
        row_number() over()\
        ,CASE WHEN ticketnumber SIMILAR TO '\d*'\
            THEN CAST(ticketnumber AS BIGINT)\
            ELSE NULL\
        END\
        ,CASE WHEN date SIMILAR TO '\d{4}-\d{2}-\d{2}'\
            THEN TO_DATE(date, 'YYYY-MM-DD')\
            ELSE NULL\
        END\
        ,CASE WHEN isbn SIMILAR TO '\d*'\
            THEN CAST(isbn AS BIGINT)\
            ELSE NULL\
        END\
        ,store\
        FROM fbde.ventes;"
    )
    cur.execute(
        "INSERT INTO fbdt.CATALOGUE\
        SELECT\
        row_number() over()\
        ,CAST(ref as INTEGER)\
        ,CAST(isbn AS BIGINT)\
        ,title\
        ,authors\
        ,language\
        ,TO_DATE(pubdate, 'YYYY-MM-DD')\
        ,publisher\
        ,tags\
        ,genre\
        FROM fbde.CATALOGUE;"
    )
    cur.execute(
        "INSERT INTO fbdt.Insee\
        SELECT\
        CAST(departementcode AS INTEGER)\
        ,departementLabel\
        ,CAST(population AS INTEGER)\
        FROM fbde.Insee;"
    )
    cur.execute(
        "INSERT INTO fbdt.Marketing\
        SELECT\
        row_number() over()\
        ,magasin\
        ,CAST(departement as INTEGER)\
        ,rayonnageCode\
        ,rayonnageLabel\
        ,RayonBS::BOOLEAN\
        ,RayonR::BOOLEAN\
        FROM fbde.Marketing;"
    )

    conn.commit()



def tuple_to_list(tuple_init, update_list):
    for x in tuple_init:
        update_list.append(x)
    return update_list


def fbdt_transform_data_Insee(conn, cur):

    cur.execute("Select Count(*) from fbdt.insee;")
    row = cur.fetchone()
    num_lines = row[0]

    cur_update = conn.cursor()
    cur.execute("Select * from fbdt.insee")

    nb_row_updated = 0
    ru = re.compile("0*\.")
    for row in cur:
        up_row = []
        up_row = tuple_to_list(row, up_row)
        if (row[0] > 0 and row[0] <= 95) or (row[0] >= 971 and row[0] <= 976):
            up_row[3] = ("" if up_row[3] is None else up_row[3]) + "0"
        else:
            up_row[3] = ("" if up_row[3] is None else up_row[3]) + "1"
            up_row[0] = None
        if row[1] is not None:
            up_row[3] = ("" if up_row[3] is None else up_row[3]) + "0"
        else:
            up_row[3] = ("" if up_row[3] is None else up_row[3]) + "1"
        if row[2] > 0 and row[2] < 60000000:
            up_row[3] = ("" if up_row[3] is None else up_row[3]) + "0"
        else:
            up_row[3] = ("" if up_row[3] is None else up_row[3]) + "1"
            up_row[2] = None

        up_row[3] = ("" if up_row[3] is None else up_row[3]) + "."

        # Update de la table
        up_row[1] = row[1].replace("'", "''")
        cur_update.execute(
             "UPDATE fbdt.insee\
                SET departementcode={},\
                departementlabel='{}',\
                population={}\
                WHERE departementcode = {}".format(up_row[0], up_row[1], up_row[2], row[0])
        )
        nb_row_updated = (
            nb_row_updated + 1 if not ru.fullmatch(up_row[3]) else nb_row_updated
        )

    cur_update.close()
    conn.commit()
    log_count_trans(cur, num_lines, "fbdt.insee")


def fbdt_transform_data_marketing(conn, cur):

    cur.execute("Select Count(*) from fbdt.marketing;")
    row = cur.fetchone()
    num_lines = row[0]

    cur_insee = conn.cursor()
    cur_update = conn.cursor()
    cur.execute("Select * from fbdt.marketing")

    nb_row_updated = 0
    ru = re.compile("0*\.")

    for row in cur:
        up_row = []
        up_row = tuple_to_list(row, up_row)
        r = re.compile("M\d{1,3}")
        if r.fullmatch(row[1]) is not None:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "0"
        else:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "1"
            up_row[1] = None

        cur_insee.execute(
            """Select * from fbdt.Insee where departementcode = %s""", [row[2]]
        )
        if (
            cur_insee.rowcount == 1 and row[2] is not None
        ):  # Exactement un departement trouve
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "0"
        else:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "1"
            up_row[2] = None

        if row[3] == "A" or row[3] == "Y" or row[3] == "E":
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "0"
        else:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "1"
            up_row[3] = None

        if (
            (row[3] == "A" and row[4] == "Author")
            or (row[3] == "E" and row[4] == "Editor")
            or (row[3] == "Y" and row[4] == "Year")
        ):
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "0"
        else:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "1"
            up_row[4] = None

        if row[5] is True or row[5] is False:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "0"
        else:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "1"
            up_row[5] = None

        if row[6] is True or row[6] is False:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "0"
        else:
            up_row[7] = ("" if up_row[7] is None else up_row[7]) + "1"
            up_row[6] = None
        # Ajout d'un caractère de fin de chaîne
        up_row[7] = ("" if up_row[7] is None else up_row[7]) + "."

        # Update de la table
        cur_update.execute(
            "UPDATE fbdt.marketing\
                SET magasin='{}',\
                departement={},\
                rayonnageCode='{}',\
                rayonnageLabel='{}',\
                RayonBS='{}',\
                RayonR='{}',\
                data_check='{}'\
                WHERE row_num = {}".format(up_row[1],up_row[2],up_row[3],up_row[4],up_row[5],up_row[6],up_row[7],up_row[0])
        )

        nb_row_updated = (
            nb_row_updated + 1 if not ru.fullmatch(up_row[7]) else nb_row_updated
        )
    cur_insee.close()
    cur_update.close()
    conn.commit()
    log_count_trans(cur, num_lines, "fbdt.marketing")


def fbdt_transform_data(conn, cur):
    cur.execute("SET search_path TO fbdt")
    fbdt_transform_data_Insee(conn, cur)
    fbdt_transform_data_marketing(conn, cur)

    # print(last_row)
