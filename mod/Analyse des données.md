# Analyse des données

Nous sommes ici en présences de 4 sources de données, sous différentes formes.

## Données

### Insee

Un fichier CSV *departementsInsee2003.txt* est présent dans les fichiers transmis, et contient la liste des départements français, avec leur numéro, code, et nombre d'habitant en 2003. Il est possible d'obtenir une version à jour de cette liste via le portail de l'Insee (fichier Insee.csv, de 2016).

### Fantastic

Le fichier *Fantastic* contient sous format CSV les ventes de l'entreprise, avec pour un numéro de ticket, la date, les livres vendus et le magasin où a eu la la vente. Le fichier 512018 lignes, et des erreurs de données.

### Marketing

Le fichier *Marketing.odt* au format openDocument, contient la liste des magasins de l'entreprise, leur organisation interne, et leur localisation. 152 lignes de données, plus un en-tête et un commentaire, ne semble pas contenir d'erreurs de données.

### Catalogue

Table dans une base de données Oracle, cette source contient la livres vendus par l'entreprise et leurs informations, soient donc une reference (clé primaire de la table), l'ISBN (Unique), le titre, les auteurs, la langue de l'ouvrage, la date de publication, l'éditeur, le genre, et ses tags. Il y a 1443 lignes dans cette table.

## Modèle Relationnel

On peut extrapoler un premier modèle relationnel à partir de ces données.

```
Fantastic(TicketNumber:Entier, Date:Date, ISBN:Chaîne, Store:Chaîne)
Marketing(Magasin:Chaîne, Rayonnage:Chaîne, RayonBestseller:Booleen, RayonRecent: Boolen, Departement:Chaîne)
Insee(Code:Entier, Nom:Chaîne, Population:Entier)
Catalogue(Ref:Entier, ISBN:Entier, Title:Chaîne, Authors:Chaîne, Language:Chaîne, Pubdate:Date, Publisher:Chaîne, Tags:Chaîne, Genre:Chaîne)
```

## Modèle Relationnel Normalisé

Une transformation nous permet d'obtenir le modèle normalisé suivant (le nom des fichiers a été gardé pour nommer les tables, mais un nom se rapprochant plus de la relation aurait pu être choisi):
```
Fantastic(#TicketNumber:Entier, Date:Date, Store=>Store)
Ventes(#TickerNumber=>Fantastic, #ISBN=>Catalogue, Quantité: Entier)
Marketing(#Magasin:Chaîne, Rayonnage:Chaîne, RayonBestseller:Booleen, RayonRecent: Boolen, Departement=>Insee)
Insee(#Code:Entier, Nom:Chaîne, Population:Entier)
Catalogue(#ISBN:Entier, Title:Chaîne, Authors:Chaîne, Language:Chaîne, Pubdate:Date, Publisher:Chaîne, Tags:Chaîne, Genre:Chaîne)
```

## UML du modèle relationnel normalisé

![UML](Modèle_relation_normalisé/Modèle_relation_normalisé.png)