# Analyse des besoins

Les besoins recueillis émanent de 2 sources différentes, à savoir la direction marketing et la direction éditoriale. Je vais ici traiter séparément ces 2 sources, d'abord en mettant sous formes de question leurs requêtes, puis en transformant ces questions en requêtes décisionnelles

## Direction Marketing
```La direction marketing est en charge de l’implantation des magasins dans les départements et de l'organisation des rayonnages (type de rangement et présence de rayons spécifiques pour les best-sellers). Elle cherche à savoir si l'organisation du rayonnage des magasins a une influence sur les volumes ventes, et si cela varie en fonction des jours de la semaine ou de certaines périodes de l'année. Elle voudrait également savoir si certains magasins ou départements sont plus dynamiques que d'autres.```

A travers ce texte, on peut arriver aux questions suivantes:
 1. L'organisation du rayonnage a-t-elle une influence sur les volumes des ventes?
 2. En supposant que cette influence existe, y a-t-il une relation avec des éléments temporels?
 3. Certains magasins ou départements sont-ils plus dynamiques que d'autres? 

On peut traduire ces questions de la manière suivante:
 1. ```
    nb de ventes
    /magasin(organisation du rayonnage)
    ```
    Il faut noter que pour mettre en évidence une influence du rayonnage sur les ventes, il faudra ramener le nombre de vente à un magasin.
 2. Cette question est une extension de la question précédente. Considérant que cette question est posée par une Direction Marketing, on peut hiérarchiser la dimension temporelle de la question ainsi:
    ```
    nb de ventes
    /magasin(organisation du rayonnage)
    /temps (jour de la semaine, mois, trimestre, semestre, année)
    ```
 3. Le mot *dynamique* peut être soumis à interprétation. Cependant, la notion de dynamise reste lié au temps. Donc une traduction de cette question sous forme de requête décisionnelle donnera le résultat suivant:
    ```
    nb de ventes
    /espace (magasin, département)
    /temps (semaine, mois, trimestre, semestre, année)
    ```

## Direction Éditoriale
```La direction éditoriale se demande si certains livres se vendent mieux à certaines dates et/ou dans certains magasins ou départements. Elle aimerait également savoir si certains auteurs ou éditeurs se vendent mieux, et s'il existe un lien entre l'ancienneté des livres et les ventes. Elle se demande aussi si certaines périodes sont plus propices que d'autres à l'écoulement des livres les plus anciens.```

On arrive aux questions suivantes:
 1. Certains livres se vendent-ils à certaines dates et/ou dans certains magasins ou département ?
 2. Certains auteurs ou éditeurs se vendent-ils mieux?
 3. Y a-t-il un lien entre l'ancienneté des livres et leurs ventes?
 4. Les livres les plus anciens se vendent-ils mieux à certaines périodes de l'année?

On peut traduire ces questions de la manière suivante:
 1. ```
    nb de ventes
    /livre(Titres)
    /temps (semaine, mois, trimestre, semestre, année)
    /espace (magasin, département)
    ```
 2. ```
    nb de ventes
    /livre(Auteur, Éditeur)
    ```
 3. ```
    nb de ventes
    /livre(date de publication)
    /temps(semaine, mois, année)
    ```
 4. ```
    nb de ventes
    /livre(date de publication)
    /temps(mois, trimestre, semestre)
    ```